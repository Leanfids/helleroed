﻿using System.Configuration;
using Devart.Data.Oracle;

namespace HelleRoedFakPdf
{
    public class DatabaseGlobalization
    {
        public static string GetConnection()
        {
            var appSettings = ConfigurationManager.AppSettings;
            OracleConnectionStringBuilder oraCSB =
                new OracleConnectionStringBuilder
                {
                    ConnectionString = appSettings["ConnectionString"],
                    InitializationCommand =
                        "ALTER SESSION SET NLS_TERRITORY='DENMARK' NLS_LANGUAGE='DANISH' NLS_DATE_LANGUAGE='DANISH' NLS_CURRENCY='kr.' NLS_DATE_FORMAT='DD-MM-YYYY' NLS_ISO_CURRENCY='DENMARK' NLS_NUMERIC_CHARACTERS=',.' NLS_DUAL_CURRENCY='kr.' NLS_NCHAR_CONV_EXCP='True' NLS_TIMESTAMP_FORMAT='DD-MM-YYYY HH24:MI:SS' NLS_TIMESTAMP_TZ_FORMAT='DD-MM-YYYY HH24:MI:SS TZH:TZM' TIME_ZONE='Europe/Copenhagen'"
                };
            return oraCSB.ConnectionString;
        }
    }
}
