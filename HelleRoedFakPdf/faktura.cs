﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using HELLEROEDContext;
using NameParser;
using NLog;
using OpenHtmlToPdf;

namespace HelleRoedFakPdf
{
    public class Faktura
    {
        // kommentar
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public string GetMailMessageHtml(string forNavn, decimal fakturaNummer)
        {
            var mySb = new StringBuilder();

            using (var mOtc = new OracleTrackedConnection(null, true))
            {
                var myFakLineToSend = from myFakturaLinjer in mOtc.InnerDbContext.FAKTURALINJERs
                    where myFakturaLinjer.FAKTURALINJE2FAKTURAHOVED == fakturaNummer
                          && (myFakturaLinjer.KURSUSID ?? 0) != 0
                    select myFakturaLinjer;

                var antalKurser = myFakLineToSend.Count();
                
                mySb.AppendLine("<span style='font-size:10.0pt;line-height:115%;font-family:Verdana'>");
                mySb.AppendLine("Hej " + forNavn + "<br><br>");

                switch (antalKurser)
                {
                    case 0:
                        mySb.AppendLine("Hermed fremsendes vedhæftede faktura.<br><br>");
                        break;
                    case 1:
                        mySb.AppendLine("Tak for din tilmelding til nedenstående kursus<br><br>");
                        break;
                    default:
                        mySb.AppendLine("Tak for din tilmelding til nedenstående kurser<br><br>");
                        break;
                }

                foreach (var fakturalinje in myFakLineToSend)
                {
                    var kursus = (from dbKursus in mOtc.InnerDbContext.KURSUs
                        where dbKursus.KURSUSID == fakturalinje.KURSUSID
                        select dbKursus).SingleOrDefault();

                    if (kursus != null)
                    {
                        mySb.AppendLine("<b>" + kursus.KURSUSBESKRIVELSE + "</b> der afholdes <b>" +
                                        kursus.KURSUSPERIODE + "</b><br><br>");
                    }
                }

                switch (antalKurser)
                {
                    case 0:
                        break;
                    case 1:
                        mySb.AppendLine(
                            "Du vil lige inden kurset modtage en deltagerliste og en kørselsbeskrivelse.<br><br>");
                        break;
                    default:
                        mySb.AppendLine(
                            "Du vil lige inden det enkelte kursus afholdes modtage en deltagerliste og en kørselsbeskrivelse.<br><br>");
                        break;
                }

                mySb.AppendLine("Med venlig hilsen<br>Helle Roed<br>");
                mySb.AppendLine("______________________________ <br>");
                mySb.AppendLine("</span><span style='font-family:Verdana;color:#9F8ADA'; font-size:10.0pt>");
                mySb.AppendLine("Zoneterapi og Kursuscenteret Helle Roed<br>");
                mySb.AppendLine(
                    "</span><span style='font-size:9.0pt;line-height:115%;font-family:Verdana;color:#9F8ADA'>");
                mySb.AppendLine("Majsmarken 1, DK-9500 Hobro<br>");
                mySb.AppendLine("Telefon: 40 79 56 62<br>");
                mySb.AppendLine(
                    "Hjemmeside <a href='http://www.helleroed.dk' target='_blank'>http://www.helleroed.dk</a><br><br>");
                mySb.AppendLine("</span><span style='font-size:8.0pt;line-height:115%;font-family:Verdana'>");
                mySb.AppendLine(
                    "Kursuscenteret Helle Roed har til formål at tilbyde attraktive efteruddannelses kurser med dygtige undervisere med ");
                mySb.AppendLine(
                    "varierende indhold. Kurserne kan bruges som en god inspiration til hverdagen på klinikken.</span>");
            }
            return mySb.ToString();
        }
        public MemoryStream GenerateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(value ?? ""));
        }
        public void email_send(string receiver, string pdfFileName, string htmlMail, byte[] pdfBytes)
        {
            try
            {
            var mail = new MailMessage();
            var smtpServer = new SmtpClient("192.168.202.151");
            mail.From = new MailAddress("helle@helleroed.dk");
            mail.To.Add(receiver);
            mail.Bcc.Add("helle@helleroed.dk");
            mail.Subject = "Tilmeldingskvittering og faktura";
            mail.Body = htmlMail;
            mail.IsBodyHtml = true;
            
            var stream = new MemoryStream(pdfBytes);
            var attachment = new Attachment(stream, MediaTypeNames.Application.Octet);
            var disposition = attachment.ContentDisposition;
            disposition.CreationDate = DateTime.Now;
            disposition.ModificationDate = DateTime.Now;
            disposition.ReadDate = DateTime.Now;
            disposition.FileName = pdfFileName;
            disposition.Size = pdfFileName.Length;
            disposition.DispositionType = DispositionTypeNames.Attachment;
            
            mail.Attachments.Add(attachment);

            var mywc = new WebClient();
            byte[] handelsbetingelser =
                mywc.DownloadData("http://www.helleroed.dk/CustomerCode/helleroed/HelleRoedHandelsbetingelser.pdf");
            stream = new MemoryStream(handelsbetingelser);
            attachment = new Attachment(stream, MediaTypeNames.Application.Octet);
            disposition = attachment.ContentDisposition;
            disposition.CreationDate = DateTime.Now;
            disposition.ModificationDate = DateTime.Now;
            disposition.ReadDate = DateTime.Now;
            disposition.FileName = "HelleRoedHandelsbetingelser.pdf";
            disposition.Size = pdfFileName.Length;
            disposition.DispositionType = DispositionTypeNames.Attachment;
            
            mail.Attachments.Add(attachment);

            smtpServer.Port = 25;
            //smtpServer.Credentials = new System.Net.NetworkCredential("your mail@gmail.com", "your password");
            //smtpServer.EnableSsl = true;

            smtpServer.Send(mail);
            stream.Dispose();
            attachment.Dispose();
            smtpServer.Dispose();
            mail.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        private void LavTilmelding(decimal fakturaNummer)
        {
            using (var mOtc = new OracleTrackedConnection(null, true))
            {
                var myFakLineToSend = from dbFakturaHoved in mOtc.InnerDbContext.FAKTURAHOVEDs
                    join dbFakturaLinjer in mOtc.InnerDbContext.FAKTURALINJERs on dbFakturaHoved.FAKTURANUMMER equals
                    dbFakturaLinjer.FAKTURALINJE2FAKTURAHOVED
                    where dbFakturaHoved.FAKTURANUMMER == fakturaNummer
                          && (dbFakturaLinjer.KURSUSID ?? 0) != 0
                          && (dbFakturaLinjer.PRISIALT ?? 0) > 0
                    select new {dbFakturaHoved, dbFakturaLinjer};

                foreach (var fakturalinjer in myFakLineToSend)
                {
                    if (!mOtc.InnerDbContext.KURSISTER2KURSUs.Any(x => x.FAKTURAFK == fakturaNummer))
                    {
                        var myKursusTilmeld = new KURSISTER2KURSU
                        {
                            KURSUSFK = fakturalinjer.dbFakturaLinjer.KURSUSID,
                            CREATEDDATE = DateTime.Now,
                            FAKTURAFK = fakturalinjer.dbFakturaHoved.FAKTURANUMMER,
                            KURSISTERFK = fakturalinjer.dbFakturaHoved.FAKTURAHOVED2KURSIST ?? 0,
                            TILMELDINGSSTATUS = "TILMELDT"
                        };
                        mOtc.InnerDbContext.KURSISTER2KURSUs.InsertOnSubmit(myKursusTilmeld);
                        mOtc.InnerDbContext.SubmitChanges();
                    }
                    else
                    {
                        _logger.Warn("Kursist allerede tilmeldt kursus en gang...");
                    }
                }
            }
        }
        
        public byte[] PdfSharpConvert(string html)
        {
           var pdf = Pdf
                    .From(html)
                    .OfSize(PaperSize.A4)
                    .WithTitle("Title")
                    .WithoutOutline()
                    .WithMargins(1.25.Centimeters())
                    .Portrait()
                    .Comressed()
                    .Content();

            return pdf;
        }

        public string LoadPage(string url)
        {

            var req = WebRequest.Create(url);
            req.Method = "GET";

            string source;
            using (var reader = new StreamReader(req.GetResponse().GetResponseStream()))
            {
                source = reader.ReadToEnd();
            }
            
            return source;
        }


        public void UpdateFakturaStatus(decimal fakturanummer, string status)
        {
            using (var mOtc = new OracleTrackedConnection(null, true))
            {
                var myFakProcessed = (from myFakturaHoved in mOtc.InnerDbContext.FAKTURAHOVEDs
                    where myFakturaHoved.FAKTURANUMMER == fakturanummer
                    select myFakturaHoved).Single();

                myFakProcessed.BETALINGSSTATUS = status;
                mOtc.InnerDbContext.SubmitChanges();
                _logger.Info($"Har opdateret faktura status til {status}");
            }
        }


        public List<FakturaToProcess> GetInvoiceCandidates()
        {
            var fakturaProcessQueue = new List<FakturaToProcess>();
            using (var mOtc = new OracleTrackedConnection(null, true))
            {
                var invoiceCodes = from myInvoiceCodes in mOtc.InnerDbContext.TILMELDINGSKODERs
                    select myInvoiceCodes;

                var myFakToSend = from myFakturaHoved in mOtc.InnerDbContext.FAKTURAHOVEDs
                    where myFakturaHoved.BETALINGSSTATUS == "KlarEmail"
                          || myFakturaHoved.BETALINGSSTATUS == "BetKlarEmail"
                    select myFakturaHoved;

                foreach (var fakturahoved in myFakToSend)
                {

                    var getCurrentInvoiceCode =
                        invoiceCodes.Single(x => x.TILMELDINGSKODE == fakturahoved.BETALINGSHOVED);

                    var myFakturaToProcess = new FakturaToProcess
                    {
                        UdsendFaktura = getCurrentInvoiceCode.INKLFAKTURA == "J",
                        betaltFaktura = fakturahoved.BETALINGSSTATUS == "BetKlarEmail",
                        fakturanummer = fakturahoved.FAKTURANUMMER,
                        fakturaSeekid = fakturahoved.SEEKID,
                        fakturaNavn = fakturahoved.NAVN,
                        fakturaEmail = fakturahoved.EMAIL
                    };

                    fakturaProcessQueue.Add(myFakturaToProcess);

                }
            }
            return fakturaProcessQueue;
        }


        public void CheckAndSendInvoice()
        {
            try
            {
                var fakturaProcessQueue = GetInvoiceCandidates();

                foreach (var fakturaToProcess in fakturaProcessQueue)
                {
                    UpdateFakturaStatus(fakturaToProcess.fakturanummer,
                        (fakturaToProcess.betaltFaktura ? "BetKlarEmailProcessing" : "KlarEmailProcessing"));

                    _logger.Info($"Found an invoice ({fakturaToProcess.fakturanummer} to {fakturaToProcess.fakturaEmail}) to process ");

                    if (fakturaToProcess.UdsendFaktura)
                    {
                        var urlstring = "http://www.helleroed.dk/CustomerCode/helleroed/htmlfakturapdf.php?id="
                                        + fakturaToProcess.fakturaSeekid
                                        + "&header=Y";

                        var pdfdoc = PdfSharpConvert(LoadPage(urlstring));

                        var name = new HumanName(fakturaToProcess.fakturaNavn);
                        _logger.Info("Generate email");
                        var htmlMail = GetMailMessageHtml(name.First, fakturaToProcess.fakturanummer);
                        _logger.Info("Sending email");
                        email_send(fakturaToProcess.fakturaEmail, "Faktura_" + fakturaToProcess.fakturanummer + ".pdf",
                            htmlMail, pdfdoc);
                        _logger.Info("Generate subscription");
                    }
                    else
                    {
                        _logger.Info("Not sending emails");
                    }

                    LavTilmelding(fakturaToProcess.fakturanummer);

                    UpdateFakturaStatus(fakturaToProcess.fakturanummer, (fakturaToProcess.betaltFaktura ? "BetAuth" : "Oprettet"));

                    _logger.Info("Done handling invoice");
                }

            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }
    }
}
