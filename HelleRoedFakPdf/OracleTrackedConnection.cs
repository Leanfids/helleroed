﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Devart.Data.Oracle;
using HELLEROEDContext;


namespace HelleRoedFakPdf
{
    public class OracleTrackedConnection : IDisposable 
    {
        private static List<OracleTrackedConnection>  MyOracleTrackedConnections = new List<OracleTrackedConnection>();
        public string ConnectionSource { get; set; }
        public DateTime CreateTime { get; set; }
        public bool ContextConnection { get; set; }
        public Stopwatch DiagStopwatch { get; set; }
        public OracleConnection InnerOracleConnection { get; set; }
        public HELLEROEDDataContext InnerDbContext { get; set; }

        private bool _disposed;
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetMyMethodName()
        {
            string retval = "";
            var st = new StackTrace(new StackFrame(2, true));
            var declaringType = st.GetFrame(0).GetMethod().DeclaringType;

            if (declaringType != null)
            {
                retval = declaringType.FullName + "." + st.GetFrame(0).GetMethod().Name +
                                "(" + st.GetFrame(0).GetFileLineNumber() + ")";

                if (string.IsNullOrEmpty(retval))
                {
                    retval = "Ukendt metodekald";
                }
            }
            return retval;
        }
        public OracleTrackedConnection()
        {
            this._disposed = false;
            ConnectionSource = GetMyMethodName();
            CreateTime = DateTime.Now;
            ContextConnection = false;
            InnerOracleConnection = new OracleConnection { ConnectionString = DatabaseGlobalization.GetConnection() };
            InnerOracleConnection.ConnectionString = DatabaseGlobalization.GetConnection();
            InnerOracleConnection.Open();
            DiagStopwatch = new Stopwatch();
            DiagStopwatch.Start();
            lock (MyOracleTrackedConnections)
            {
                MyOracleTrackedConnections.Add(this);
            }
        }

        public OracleTrackedConnection(OracleTrackedConnection baseConnection, bool isContext)
        {
            if (baseConnection == null || baseConnection._disposed)
            {
                this._disposed = false;
                ConnectionSource = GetMyMethodName();
                CreateTime = DateTime.Now;
                ContextConnection = true;
                lock (MyOracleTrackedConnections)
                {
                    MyOracleTrackedConnections.Add(this);
                }
                InnerOracleConnection = new OracleConnection {ConnectionString = DatabaseGlobalization.GetConnection()};
                InnerOracleConnection.Open();
                InnerDbContext = new HELLEROEDDataContext(InnerOracleConnection);
            }
            else
            {
                this._disposed = false;
                ConnectionSource = GetMyMethodName();
                CreateTime = DateTime.Now;
                ContextConnection = false;
                InnerOracleConnection = baseConnection.InnerOracleConnection;
                DiagStopwatch = new Stopwatch();
                DiagStopwatch.Start();
            }
        }

        public OracleTrackedConnection(bool isContext)
        {
            this._disposed = false;
            ConnectionSource = GetMyMethodName();
            CreateTime = DateTime.Now;
            ContextConnection = true;
            lock (MyOracleTrackedConnections)
            {
                MyOracleTrackedConnections.Add(this);
            }
            InnerOracleConnection = new OracleConnection {ConnectionString = DatabaseGlobalization.GetConnection()};
            InnerOracleConnection.Open();
            InnerDbContext = new HELLEROEDDataContext(InnerOracleConnection);
        }

        public T BeginTrans<T> ()
        {
            if (ContextConnection)
            {
                return (T) Convert.ChangeType(InnerDbContext.Connection.BeginTransaction(), typeof (T));
            }
            else
            {
                return (T) Convert.ChangeType(InnerOracleConnection.BeginTransaction(), typeof (T));
            }
        }
        
        public HELLEROEDDataContext GetDbContext()
        {
            ContextConnection = true;
            InnerDbContext = new HELLEROEDDataContext(InnerOracleConnection);
            return InnerDbContext;
        }
        
        public override string ToString()
        {
            return "Connection created:" + CreateTime.ToString("dd-MM-yyyy hh:mm:ss") + " " + ConnectionSource + " " + DiagStopwatch.Elapsed.TotalSeconds;
        }

        public void Dispose()
        {
            if (ContextConnection)
            {
                InnerDbContext?.Dispose();
                InnerDbContext = null;
            }
            InnerOracleConnection?.Dispose();
            InnerOracleConnection = null;

            if (!this._disposed)
            {
                lock (MyOracleTrackedConnections)
                {
                    MyOracleTrackedConnections.Remove(this);
                }
            }
            this._disposed = true;
        }

        public static int Count
        {
            get
            {
                lock (MyOracleTrackedConnections)
                {
                    return MyOracleTrackedConnections.Count;
                }
            }
        }

        public static IEnumerable<OracleTrackedConnection> ConnectionList
        {
            get
            {
                lock (MyOracleTrackedConnections)
                {
                    List<OracleTrackedConnection> cloneOfMyList = new List<OracleTrackedConnection>(new List<OracleTrackedConnection>(MyOracleTrackedConnections));
                    return cloneOfMyList;
                }
            }
        }
    }
}
