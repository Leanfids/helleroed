﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelleRoedFakPdf
{
    public class FakturaToProcess
    {
        public bool betaltFaktura { get; set; }
        public decimal fakturanummer { get; set; }
        public string fakturaSeekid { get; set; }
        public string fakturaNavn { get; set; }
        public string fakturaEmail { get; set; }
        public bool UdsendFaktura { get; set; }
    }
}
