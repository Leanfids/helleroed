﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using HelleRoedFakPdf;

namespace HelleRoedFakturaService
{
    public class TaskRunner
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public bool RunTask { get; set; }

        public TaskRunner()
        {
            RunTask = true;
        }

        public void Runner()
        {
            _logger.Info("Application thread started");
            while (RunTask)
            {
                var fak = new Faktura();
                fak.CheckAndSendInvoice();
                Thread.Sleep(30000);
            }
        }
    }
}
