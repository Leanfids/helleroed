﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using CommandLineParser;
using CommandLineParser.Arguments;
using CommandLineParser.Exceptions;
using NLog;

namespace HelleRoedFakturaService
{
    static class Program
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            _logger.Info("Program is started");

            try
            {
                var parser = new CommandLineParser.CommandLineParser();
                //switch argument is meant for true/false logic

                var isRunningAsCommandline = new SwitchArgument('c', "commandline", "Is runned in the commandline", false);
                // ValueArgument<decimal> version = new ValueArgument<decimal>('v', "version", "Set desired version");
                //EnumeratedValueArgument<string> color = new EnumeratedValueArgument<string>('c', "color", new string[] { "red", "green", "blue" });

                parser.Arguments.Add(isRunningAsCommandline);
                parser.ParseCommandLine(args);
                parser.ShowParsedArguments();


                if (isRunningAsCommandline.Value)
                {
                    _logger.Info("as commandline tool...");

                    var fak = new HelleRoedFakPdf.Faktura();
                    fak.CheckAndSendInvoice();
                }
                else
                {
                    var servicesToRun = new ServiceBase[]
                    {
                        new HelleRoedFakturaService()
                    };
                    ServiceBase.Run(servicesToRun);
                }

            }
            catch (CommandLineException e)
            {
                Console.WriteLine(e.Message);
                _logger.Error(e.Message);
            }
        }
    }
}
