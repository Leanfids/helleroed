﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace HelleRoedFakturaService
{
    public partial class HelleRoedFakturaService : ServiceBase
    {
        private Logger _logger = LogManager.GetCurrentClassLogger();

        TaskRunner _myTaksRunner;
        public HelleRoedFakturaService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _logger.Info("Application started");
            _myTaksRunner = new TaskRunner();

            var myRunThread = new Thread(_myTaksRunner.Runner);
            myRunThread.Start();
            
        }

        protected override void OnStop()
        {
            _logger.Info("Application stopped signal received");
            _myTaksRunner.RunTask = false;
        }
    }
}
